import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SimpleFoodOrderingMachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane receivedInfo;
    private JButton stewButton;
    private JButton curryButton;
    private JButton bibimbapButton;
    private JLabel topLabel;
    private JTextArea textArea2;
    private JButton checkOutButton;
    private JButton cancelButton;
    private  JLabel Totalcash;
    public int Totalcost=0;
    private String textArea1 = textArea2.getText();


    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new SimpleFoodOrderingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
    public void totalcost(String Food,int cost){

        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+Food+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            Totalcost+=cost;
            textArea2.append(Food + "：" + cost + " yen\n");
            Totalcash.setText("Total cost : "+Totalcost+" yen");
            JOptionPane.showMessageDialog(null,"Thank you for ordering "+Food+"! It will be served as soon as possible. ");
        }
    }
    public SimpleFoodOrderingMachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Tempura",900);

            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Ramen",1400);
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Udon",800);
            }
        });


        stewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Stew",600);
            }
        });
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Curry",500);
            }
        });
        bibimbapButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                totalcost("Bibimbap",850);
            }
        });
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Cancel?",
                        "Cancel Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    textArea2.setText(textArea1);
                    Totalcost = 0;
                    Totalcash.setText("Total cost : " + Totalcost + " yen");

                }
            }
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to order Checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    textArea2.setText("");

                    JOptionPane.showMessageDialog(null, "Thank you. The total price is " +Totalcost+ "yen.");
                    textArea2.setText(textArea1);
                    Totalcost = 0;
                    Totalcash.setText("Total cost : "+Totalcost+" yen");
                }
            }

        });
    }
}

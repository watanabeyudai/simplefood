import java.util.Scanner;
public class Exercise1 {
    public static void main(String[] args) {
        Scanner userIn = new Scanner(System.in);
        System.out.println("Hello,World");
        System.out.println("what would you like to order:");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");
        System.out.println("Your order[1-3]:");
        int number = userIn.nextInt();
        userIn.close();
        if (number == 1) {
            System.out.println("You have ordered Tempura.Thank you!");
        }
        if (number == 2) {
            System.out.println("You have ordered Ramen.Thank you!");
        }
        if (number == 3) {
            System.out.println("You have ordered Udon.Thank you!");
        }
    }
}
